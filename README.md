# How to add OpenAPI to ASP.NET Core project

**To read**: <https://codingstories.io/story/https:%2F%2Fgitlab.com%2Fcodingstories%2Fhow-to-add-openapi-to-aspnetcore>

**Estimated reading time**: 15 min

## Story Outline

This story will tell you how to add OpenAPI support to your ASP.NET Core project.

## Story Organization

**Story Branch**: main
> `git checkout main`

Tags: #csharp, #aspnetcore, #openapi
