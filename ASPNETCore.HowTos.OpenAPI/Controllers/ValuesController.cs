﻿namespace ASPNETCore.HowTos.OpenAPI.Controllers
{
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Values Controller V1
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/values")]
    public class ValuesController : ControllerBase
    {
        /// <summary>
        /// GET V1
        /// </summary>
        /// <param name="apiVersion"></param>
        /// <remarks>
        /// Sample request:
        ///
        ///     GET api/values?api-version=2.0
        ///
        /// </remarks>
        /// <returns></returns>
        [HttpGet]
        public string Get(ApiVersion apiVersion) =>
            $"Controller = {this.GetType().Name}\nVersion = {apiVersion}";
    }
}
