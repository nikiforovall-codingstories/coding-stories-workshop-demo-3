# 1. Initial Test

Install `httprepl` by running:

`dotnet tool install -g Microsoft.dotnet-httprepl`

```bash
$ httprepl.exe https://localhost:5001/
(Disconnected)> connect https://localhost:5001/
Using a base address of https://localhost:5001/
Unable to find an OpenAPI description
For detailed tool info, see https://aka.ms/http-repl-doc
```

`https://localhost:5001/> GET api/values`

Response:

```http
HTTP/1.1 200 OK
api-supported-versions: 1.0, 2.0
Content-Type: text/plain; charset=utf-8
Date: Sun, 13 Jun 2021 12:02:32 GMT
Server: Kestrel
Transfer-Encoding: chunked

Controller = Values2Controller
Version = 2.0
```

`https://localhost:5001/> GET api/values?api-version=1.0`

Response:

```http
HTTP/1.1 200 OK
api-supported-versions: 1.0, 2.0
Content-Type: text/plain; charset=utf-8
Date: Sun, 13 Jun 2021 12:06:40 GMT
Server: Kestrel
Transfer-Encoding: chunked

Controller = ValuesController
Version = 1.0
```
