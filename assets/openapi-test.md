# 2. OpenAPI Test

Connect:

```bash
$ httprepl.exe https://localhost:5001/ --openapi https://localhost:5001/swagger/v2/swagger.json
(Disconnected)> connect https://localhost:5001/ --openapi https://localhost:5001/swagger/v2/swagger.json
Checking https://localhost:5001/swagger/v2/swagger.json... Found
Parsing... Successful

Using a base address of https://localhost:5001/
Using OpenAPI description at https://localhost:5001/swagger/v2/swagger.json
For detailed tool info, see https://aka.ms/http-repl-doc
```

Discover:

```bash
https://localhost:5001/> ls
.     []
api   []

https://localhost:5001/> cd api
/api    []

https://localhost:5001/api> ls
.        []
..       []
values   [GET]
```

Run:

`https://localhost:5001/api> GET values`

Response:

```http
HTTP/1.1 200 OK
api-supported-versions: 1.0, 2.0
Content-Type: text/plain; charset=utf-8
Date: Sun, 13 Jun 2021 15:57:41 GMT
Server: Kestrel
Transfer-Encoding: chunked

Controller = Values2Controller
Version = 2.0
```
